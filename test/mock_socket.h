#include "gmock/gmock.h"
#include "../src/ISocket.h"

class MockSocket : public ISocket{
	public:
	MOCK_METHOD1(send, size_t(const boost::asio::const_buffer& buffers));
	MOCK_METHOD0(get_executor, boost::asio::executor());
	MOCK_METHOD2(async_write_some, void(const boost::asio::const_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler));
	MOCK_METHOD2(async_read_some, void(const boost::asio::mutable_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler));
	MOCK_METHOD0(close, void());
	MOCK_METHOD0(is_open, bool());


};
