#include "gmock/gmock.h"
#include "../src/IConnection.h"
class MockIConnection : public IConnection {
 public:
  MOCK_METHOD2(do_readHeader,
      void(ISocket& socket, Message& msg));
  MOCK_METHOD2(do_readBody,
      void(ISocket& socket, Message& msg));
  MOCK_METHOD0(do_writeText,
      void());
  MOCK_METHOD0(do_writeFile,
      void());
  MOCK_METHOD2(writeFileCallback,
      void(const boost::system::error_code& ec, std::size_t));
  MOCK_METHOD1(removeFileFromQueue,
      void(size_t fileNumber));
  MOCK_METHOD0(pauseFileTransfer,
      void());
  MOCK_METHOD0(resumeFileTransfer,
      void());
  MOCK_METHOD0(printFileQueue,
      void());
  MOCK_METHOD0(close,
      void());
  MOCK_METHOD0(isOpen,
      bool());
  MOCK_METHOD1(write,
      void(TextMessage));
  MOCK_METHOD1(sendFile,
      void(std::string filePath));
  MOCK_METHOD0(async_readMessage,
      void());
};
