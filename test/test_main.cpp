#include "../src/ISocket.h"
#include "../src/Connection.h"
#include "../src/ConnectionFactory.h"
#include "../src/TextMessage.h"
#include "../src/FileMessage.h"
#include "../src/Acceptor.h"
#include "../src/Commands.h"
#include <boost/asio.hpp>
//#include "../include/gtest/gtest.h"
#include "gtest/gtest.h"
#include "mock_socket.h"
#include "MockIConnection.h"
#include <memory>
#include <string>
#include <utility>
using ::testing::AtLeast;

/*class ConnectionTest : public ::testing::Test{
	protected:
		void SetUp() override {
			auto socketPtr = std::make_unique<MockSocket>();
			EXPECT_CALL(*socketPtr, async_write_some).Times(AtLeast(1));
			EXPECT_CALL(*socketPtr, send).Times(1);
		//	c = Connection(std::move(socketPtr));
		}

		// void TearDown() override {}
		//MockSocket sock;
		Connection c;
		
};

TEST_F(ConnectionTest, simple){
//	c.async_sendMessage("test");	
//	c.sendMessage("test");	
}*/

TEST(TextMessage_test, basic){
	TextMessage msg("test");
	EXPECT_EQ(msg.getLength(), 4);
	EXPECT_EQ(msg.getBody(), "test");
}

TEST(FileMessage_test, basic){
	FileMessage fmsg;
	fmsg.setBody("test test");
	EXPECT_EQ(fmsg.getLength(), 9);
	EXPECT_EQ(fmsg.getBody(), "test test");
}

/*TEST(Commands_test, basic){
//	auto c = std::make_unique<MockIConnection>();
	boost::asio::io_context io;
	Connection c(&io, std::make_unique<MockSocket>(), std::make_unique<MockSocket>());
	Commands cmd(io, c);	
	EXPECT_CALL(c, pauseFileTransfer).Times(1);
	cmd.parseCommand("\\pause");
	EXPECT_CALL(c, resumeFileTransfer).Times(1);
	cmd.parseCommand("\\resume");
	EXPECT_CALL(c, printFileQueue).Times(1);
	cmd.parseCommand("\\ls");
	EXPECT_CALL(c, isOpen).Times(1);
	cmd.parseCommand("\\connect 127.0.0.1");
	EXPECT_CALL(c, removeFileFromQueue).Times(1);
	cmd.parseCommand("\\rm 1");
}*/

TEST(Connection_test, basic){
	boost::asio::io_context io;
	auto fileSocket = std::make_unique<MockSocket>();
	auto textSocket = std::make_unique<MockSocket>();

	EXPECT_CALL(*textSocket, async_write_some).Times(AtLeast(1));
	EXPECT_CALL(*textSocket, async_read_some).Times(AtLeast(1));
	EXPECT_CALL(*fileSocket, async_read_some).Times(AtLeast(1));
	EXPECT_CALL(*textSocket, close).Times(1);
	EXPECT_CALL(*fileSocket, close).Times(1);
	EXPECT_CALL(*fileSocket, is_open).Times(1);
	//EXPECT_CALL(*textSocket, is_open).Times(1); Short-circuit evaluation: won't be called. 

	Connection c(&io, std::move(textSocket), std::move(fileSocket));
	c<<"test";
	c.async_readMessage();
	c.isOpen();
	c.close();
	io.run();
	
}
