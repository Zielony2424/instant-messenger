#ifndef ICONNECTIONFACTORY_H
#define ICONNECTIONFACTORY_H
#include <boost/asio.hpp>
#include "SocketAdapter.h"
#include "Connection.h"

class IConnectionFactory{
	public:
	IConnectionFactory(boost::asio::io_context& io_context_);
	protected:
	boost::asio::io_context& io_context_;
	tcp::resolver::iterator adressToIterator(std::string adress, std::string port);
	std::unique_ptr<ISocket> socketToISocket(tcp::socket& socket);
};

#endif
