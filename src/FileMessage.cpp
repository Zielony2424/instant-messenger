#include "FileMessage.h"

//FileMessage::FileMessage(std::string fileContent) : Message(fileContent){
//}

FileMessage::FileMessage() : Message(){
}

std::string FileMessage::extractFilename(){
	const char zero = 0;
	auto firstNonZero = body_.find_first_not_of(zero, 0);	
	std::string result;
	std::copy(body_.begin() + firstNonZero, body_.end(), std::back_inserter(result));
	return result;
}

void FileMessage::saveFile(){
	fileName_ = extractFilename();
	boost::filesystem::path path(fileName_);	
	boost::filesystem::ofstream ofs(path, std::ios::binary);
	std::string fileBody = getBody();
	std::copy(
			fileBody.begin() + fileNameSize,
			fileBody.end(),
			std::ostreambuf_iterator<char>(ofs));
}

bool FileMessage::loadFile(std::string filePath){
	if(!boost::filesystem::exists(filePath)){
		std::cout<<filePath<<" doesn't exist\n";
		return false;
	}
	std::cout<<"Loading file...\n";
	boost::filesystem::path path(filePath);	
	fileName_ = path.filename().string();
	assert(fileName_.size() <= fileNameSize);
	boost::filesystem::ifstream ifs(path, std::ios::binary);
	std::string fileBody;
	std::copy(
			std::istreambuf_iterator<char>(ifs), 
			std::istreambuf_iterator<char>( ),
			std::back_inserter(fileBody));
	setBody(fileName_ + std::string(fileNameSize - fileName_.size(), 0) + fileBody);
	std::cout<<"Loaded: "<<fileName_<<std::endl;
	return true;
}

std::string FileMessage::getFileName(){
	return fileName_;
}

void FileMessage::show(){
	saveFile();
}
