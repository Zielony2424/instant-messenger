#ifndef CONNECTIONFACTORY_H
#define CONNECTIONFACTORY_H
#include <boost/asio.hpp>
#include <memory>
#include <utility>
#include "Connection.h"
#include "IConnectionFactory.h"
#include <iostream>
#include "SocketAdapter.h"

using namespace boost::asio::ip;

class ConnectionFactory : public IConnectionFactory{
	public:
	ConnectionFactory(boost::asio::io_context& io_context);
	Connection getConnection(std::string adress);
};

#endif
