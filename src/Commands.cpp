#include "Commands.h"

//void connectTo(std::string);

Commands::Commands(boost::asio::io_context& io_context, Connection& connection) :
	io_context_(io_context), c(connection){
	//std::map<std::string, std::function<void(std::string)>> commands {
	commands = {
		{"\\connect", [this](std::string arg){this->connectTo(arg);}},
		{"\\listen", [this](std::string arg){this->listen(arg);}},
		{"\\help", [this](std::string arg){this->showHelp(arg);}},
		{"\\h", [this](std::string arg){this->showHelp(arg);}},
		{"\\send", [this](std::string arg){this->sendFile(arg);}},
		{"\\ls", [this](std::string arg){this->ls(arg);}},
		{"\\rm", [this](std::string arg){this->rm(arg);}},
		{"\\pause", [this](std::string arg){this->pause(arg);}},
		{"\\resume", [this](std::string arg){this->resume(arg);}},
		{"\\disconnect", [this](std::string arg){this->disconnect(arg);}}

	};
}
void Commands::showHelp(std::string){
	std::cout << R"EOF_(
	\h or \help: Show this help.
	\connect <adress>: Connect to adress.
	\listen: Wait for incoming connection.
	\send <pathToFile>: Send selected file.
	\ls: List file queue.
	\rm <fileNumber>: Remove file form the queue.
	\pause: Pause file transfer.
	\resume: Resume file transfer.
	\disconnect:
)EOF_";
}

void Commands::sendFile(std::string filePath){
	if(c.isOpen()){
		c.sendFile(filePath);	
	}
	else{
		std::cout<<"You need to be connected. Use \\connect <adress> to connect or \\listen to wait for connection.\n";
	}
}

void Commands::connectTo(std::string adress){
	ConnectionFactory factory(io_context_);
	short tries = 1;
	short maxTries = 3;
	if(c.isOpen()){
		std::cout<<"You are already connected/\n";
	}
	else{
		std::cout<<"Connecting to " + adress + "\n";
		while(1){
			try{
				c = factory.getConnection(adress);
				c.async_readMessage();
				std::cout<<"Connected\n";
				return;
			}
			catch(boost::wrapexcept<boost::system::system_error>& err){
				std::cout<<"Error while connecting "<<err.what()<<"\n";
				std::cout<<"Retrying ("<<tries<<"/"<<maxTries<<")\n";
				if (tries++ == maxTries){
					std::cout<<err.what()<<"\n";
					return;
				}
			}
		}
	}
}

void Commands::listen(std::string){
	std::cout<<"Waiting for connection\n";
	auto a = Acceptor(io_context_);
	c = a.listenForConnection();
	c.async_readMessage();
}

void Commands::ls(std::string){
	c.printFileQueue();	
}

void Commands::rm(std::string fileNumber){
	try{
		auto number = std::stoull(fileNumber);
		c.removeFileFromQueue(number);
	}
	catch(std::invalid_argument& e){
		std::cout<<"Error while parsing: "<<fileNumber
			<<"\n argument should be an integer\n";
		return;
	}
}

void Commands::disconnect(std::string){
	if(c.isOpen()){
		c.close();
		std::cout<<"Disconnected\n";
	}
	else{
		std::cout<<"There is no active connection\n";	
	}
}

void Commands::resume(std::string){
	c.resumeFileTransfer();
}

void Commands::pause(std::string){
	c.pauseFileTransfer();	
}

void Commands::parseCommand(std::string command){
	std::string cmd, arg;
	std::tie(cmd, arg) = getArg(command);
	if(commands.find(cmd) == commands.end()){
		std::cout<<cmd<<": Command not found\n";
	}
	else{
		commands[cmd](arg);
	}
}

std::pair<std::string, std::string> Commands::getArg(std::string command){
	auto it = command.find(" ");	
	if(it == std::string::npos){
		return {command, ""};
	}
	else{
		return {command.substr(0, it), command.substr(it + 1, command.size()-it)};
	}
}
