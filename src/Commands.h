#include <string>
#include <thread>
#include <tuple>
#include <map>
#include <iostream>
#include <utility>
#include <boost/asio.hpp>
#include "Connection.h"
#include "IConnection.h"
#include "ConnectionFactory.h"
#include "Acceptor.h"

class Commands{
	boost::asio::io_context& io_context_;
	Connection& c;
	std::map<std::string, std::function<void(std::string)>> commands;
	void connectTo(std::string adress);
	void listen(std::string);
	void sendFile(std::string filePath);
	void showHelp(std::string);
	void ls(std::string);
	void rm(std::string fileNumber);
	void disconnect(std::string);
	void pause(std::string);
	void resume(std::string);
	std::pair<std::string, std::string> getArg(std::string command);
	public:
	Commands(boost::asio::io_context& io_context, Connection& connection);
	void parseCommand(std::string command);

};
