#include "IConnectionFactory.h"


IConnectionFactory::IConnectionFactory(boost::asio::io_context& io_context):io_context_(io_context){}

std::unique_ptr<ISocket> IConnectionFactory::socketToISocket(tcp::socket& socket){

	auto adapter = SocketAdapter(std::move(socket));
	auto adapterPtr = std::make_unique<SocketAdapter>(std::move(adapter));
	return adapterPtr;
}


tcp::resolver::iterator IConnectionFactory::adressToIterator(std::string adress, std::string port){
	tcp::resolver resolver(io_context_);
	tcp::resolver::query query(tcp::v4(), adress, port);
	tcp::resolver::iterator iterator = resolver.resolve(query);
	return iterator;
}
