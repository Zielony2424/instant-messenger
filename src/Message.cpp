#include "Message.h"

Message::Message(std::string body){
	setBody(body);
}

Message::Message(){
	length_ = 0;
	body_ = "";
	header_ = std::string(headerSize, '0');
}

void Message::prependZeros(std::string& header){
	assert(header.size()<=headerSize);
	header = std::string(headerSize - header.size(), '0') + header;
}

void Message::computeNewLength(){
	length_ = body_.size();
	header_ = std::to_string(length_);
	assert(header_.size()<=headerSize);
	prependZeros(header_);	
}

uint32_t Message::getLength() const { return length_; }

uint32_t Message::getHeaderSize() const { return headerSize; }

std::string Message::getData() const {
	return header_ + body_;
}

std::string Message::getBody() const {
	return body_;
}

char* Message::getHeaderPtr(){
	return  &header_[0];
}

char* Message::getBodyPtr(){
	return  &body_[0];
}

void Message::setData(std::string data){
	//TODO check validity od newData.
	assert(data.size()>=headerSize);
	header_ = std::string(headerSize, '0');
	std::copy(data.begin(), data.begin()+headerSize, header_.begin());
	body_ = "";
	std::copy(data.begin()+headerSize, data.end(), std::back_inserter(body_));
	length_ = body_.size();
}

void Message::setHeader(std::string header){
	assert(header.size() == headerSize);
	header_ = header;
	try{
		length_ = std::stoi(header_);
	}
	catch(std::exception& e){
		std::cerr<<"Error in setHeader: "<<e.what()<<std::endl;
	}
	body_ = std::string(length_, '0');
}

void Message::setHeader(){
	setHeader(header_);
}

void Message::setBody(std::string body){
	body_ = body;
	computeNewLength();
}

