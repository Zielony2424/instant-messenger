#ifndef MESSAGE_H
#define MESSAGE_H
#include <string>
#include <iostream>
#include <algorithm>
#include <cassert>
class Message{
	protected:
	uint32_t length_;
	std::string body_;
	std::string header_;
	void computeNewLength();
	enum {headerSize = 16};
	void prependZeros(std::string& header);
	public:
	Message(std::string body);
	Message();
	virtual void show() = 0;
	uint32_t getLength() const;
	uint32_t getHeaderSize() const;
	std::string getData() const;
	std::string getBody() const;
	char* getHeaderPtr();
	char* getBodyPtr();
	void setData(std::string);
	void setHeader(std::string header);
	void setHeader();
	void setBody(std::string body);
};

#endif
