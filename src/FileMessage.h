#ifndef FILEMESSAGE_H
#define FILEMESSAGE_H
#include "Message.h"
#include <string>
#include <cassert>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>

class FileMessage : public Message{
	std::string fileName_;
	enum{ fileNameSize = 255};
	std::string extractFilename();
	public:
	FileMessage();
	void saveFile();
	bool loadFile(std::string filePath);
	std::string getFileName();
	void show() override;
};
#endif
