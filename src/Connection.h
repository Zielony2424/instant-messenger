#ifndef CONNECTION_H
#define CONNECTION_H
#include <boost/asio.hpp>
#include <string>
#include <iostream>
#include <vector>
#include <thread>
#include <memory>
#include <functional>
#include <deque>
#include "ISocket.h"
#include "TextMessage.h"
#include "FileMessage.h"
#include "IConnection.h"

using namespace boost::asio::ip;


class Connection{
//	std::reference_wrapper<boost::asio::io_context> io_context_;
	boost::asio::io_context* io_context_;
	std::unique_ptr<ISocket> textSocket_;
	std::unique_ptr<ISocket> fileSocket_;
	std::unique_ptr<boost::asio::steady_timer> t;
	size_t bytesSent;
	bool removeFlag;
	bool pauseFlag;
	std::string buffer;
	FileMessage fileMsg;
	TextMessage textMsg;
	std::deque<TextMessage> pendingTextMessages_;
	std::deque<FileMessage> pendingFileMessages_;
	void do_readHeader(ISocket& socket, Message& msg);
	void do_readBody(ISocket& socket, Message& msg);
	void do_writeText();
	void do_writeFile();
	void writeFileCallback(const boost::system::error_code& ec, std::size_t);
	public:
	Connection(boost::asio::io_context* io_context, std::unique_ptr<ISocket> textSocket_, std::unique_ptr<ISocket> fileSocket_);
	Connection();
	Connection(Connection&& c);
	Connection(Connection&) = delete;
	Connection& operator=(Connection&& c);
	Connection& operator=(Connection&) = delete;
	void removeFileFromQueue(size_t fileNumber);
	void pauseFileTransfer();
	void resumeFileTransfer();
	void printFileQueue();
	void close();
	bool isOpen();
	void write(TextMessage);
	void sendFile(std::string filePath);
	void async_readMessage();
	void operator<<(std::string message);
};

#endif

