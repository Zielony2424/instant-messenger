#ifndef ISOCKET_H
#define ISOCKET_H
#include <boost/asio.hpp>
#include <functional>
#include <utility>

using namespace boost::asio::ip;

class ISocket{
	public:
	virtual std::size_t send(const boost::asio::const_buffer& buffers) = 0;
	virtual boost::asio::executor get_executor() = 0;
	virtual void async_write_some(const boost::asio::const_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler) = 0;
	virtual void async_read_some(const boost::asio::mutable_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler) = 0;
	virtual void close() = 0;
	virtual bool is_open() = 0;
	virtual ~ISocket() = default;
};
#endif 
