#include "Acceptor.h"

Acceptor::Acceptor(boost::asio::io_context& io_context) : IConnectionFactory(io_context){}


void Acceptor::acceptConnection(tcp::socket& socket, uint16_t port){
	tcp::acceptor acceptor(io_context_);
	tcp::endpoint endpoint(tcp::v4(), port);
	acceptor.open(endpoint.protocol());
	acceptor.set_option(tcp::acceptor::reuse_address(true));
	acceptor.bind(endpoint);
	acceptor.listen();
	acceptor.accept(socket);
}

Connection Acceptor::listenForConnection(){
	tcp::socket fileSocket(io_context_);
	tcp::socket textSocket(io_context_);
	acceptConnection(textSocket, 1338);
	acceptConnection(fileSocket, 1339);
	return Connection(&io_context_, std::move(socketToISocket(textSocket)), std::move(socketToISocket(fileSocket)));
}
