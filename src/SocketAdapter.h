#ifndef SOCKETADAPTER_H
#define SOCKETADAPTER_H
#include "ISocket.h"
#include <boost/asio.hpp>
class SocketAdapter : public ISocket{
	tcp::socket socket_;
	public:
	SocketAdapter(tcp::socket socket);
	std::size_t send(const boost::asio::const_buffer& buffers) override;
	boost::asio::executor get_executor() override;
	void async_write_some(const boost::asio::const_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler) override;
	void async_read_some(const boost::asio::mutable_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler) override;
	bool is_open() override;
	void close() override;
	
};
#endif
