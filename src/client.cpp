#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <thread>
#include <utility>
#include "Connection.h"
#include "Commands.h"

using namespace boost::asio::ip;



int main(){
	boost::asio::io_context io_context;
	boost::asio::io_context::work work(io_context);
	Connection c;
	Commands cmds(io_context, c);
	std::string nick;
	std::cout<<"Nick: ";
	std::cin>>nick;
	std::string line;
	cmds.parseCommand("\\help");
	std::thread t([&io_context](){ io_context.run(); });
	while (std::getline(std::cin, line)){
		if(line.size() && line[0] == '\\')
			cmds.parseCommand(line);
		else
		if(c.isOpen())
			c<<nick + ": " + line;
		std::cout<<(c.isOpen() ? "\n" : "\nDisconnected: ");
	}
	c.close();
	t.join();

}

