#ifndef ACCEPTOR_H
#define ACCEPTOR_H
#include <boost/asio.hpp>
#include <utility>
#include "Connection.h"
#include "SocketAdapter.h"
#include "IConnectionFactory.h"
using namespace boost::asio::ip;
class Acceptor : public IConnectionFactory{
	void acceptConnection(tcp::socket& socket, uint16_t port);
	public:
	Acceptor(boost::asio::io_context& io_context);
	Connection listenForConnection();
};
#endif
