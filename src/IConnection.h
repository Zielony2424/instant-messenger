#ifndef ICONNECTION_H
#define ICONNECTION_H
#include <boost/asio.hpp>
#include "ISocket.h"
#include <string>
#include "TextMessage.h"
#include "FileMessage.h"
#include <iostream>
#include <vector>
#include <thread>
#include <memory>
#include <functional>
#include <deque>

class IConnection{
	protected:
	virtual void do_readHeader(ISocket& socket, Message& msg)=0;
	virtual void do_readBody(ISocket& socket, Message& msg)=0;
	virtual void do_writeText()=0;
	virtual void do_writeFile()=0;
	virtual void writeFileCallback(const boost::system::error_code& ec, std::size_t)=0;
	public:
	virtual void removeFileFromQueue(size_t fileNumber)=0;
	virtual void pauseFileTransfer()=0;
	virtual void resumeFileTransfer()=0;
	virtual void printFileQueue()=0;
	virtual void close()=0;
	virtual bool isOpen()=0;
	virtual void write(TextMessage)=0;
	virtual void sendFile(std::string filePath)=0;
	virtual void async_readMessage()=0;
	virtual IConnection& operator=(IConnection&& c)=0;
};

#endif
