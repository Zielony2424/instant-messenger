#include "Connection.h"

Connection::Connection(boost::asio::io_context* io_context, std::unique_ptr<ISocket> socket, std::unique_ptr<ISocket> fileSocket) :
	io_context_(io_context), textSocket_(std::move(socket)), fileSocket_(std::move(fileSocket)){}

Connection::Connection() : io_context_(nullptr), textSocket_(nullptr), fileSocket_(nullptr){}

Connection::Connection(Connection&& c){
	*this = std::move(c);
}

Connection& Connection::operator=(Connection&& c){
	textSocket_ = std::move(c.textSocket_);
	c.textSocket_ = nullptr;
	fileSocket_ = std::move(c.fileSocket_);
	c.fileSocket_ = nullptr;
	io_context_ = std::move(c.io_context_);
	c.io_context_ = nullptr;
	return *this;
}

void Connection::printFileQueue(){
	if(pendingFileMessages_.empty())
		std::cout<<"File queue is empty.\n";
	else{
		if(pauseFlag)
			std::cout<<"File transfer if paused\n";
		for(size_t i = 0; i < pendingFileMessages_.size(); ++i){
			if(i == 0)
				std::cout<<i<<"\t"<<pendingFileMessages_[i].getFileName()<< " Sending: "
					<<100 * bytesSent / pendingFileMessages_[i].getLength()<<"%"<<std::endl;
			else
				std::cout<<i<<"\t"<<pendingFileMessages_[i].getFileName()<<std::endl;
		}	
	}
}

void Connection::removeFileFromQueue(size_t fileNumber){
	if(fileNumber>=pendingFileMessages_.size()){
		std::cout<<"There is no such file\n";
		return;
	}
/*	if(fileNumber == 0){
		removeFlag = true;
		std::cout<<"Setting removeFlag\n";
	}

	else{*/
	if(fileNumber>0){
		std::cout<<pendingFileMessages_[fileNumber].getFileName()<<" removed from queue\n";
		pendingFileMessages_.erase(pendingFileMessages_.begin() + fileNumber);
	}
}

void Connection::close(){
	boost::asio::post(*io_context_, [this](){
			textSocket_->close();
			fileSocket_->close();
			});
}

bool Connection::isOpen(){
	if(fileSocket_ == nullptr || textSocket_ == nullptr)
		return false;
	else
		return fileSocket_->is_open() && textSocket_->is_open();
}

void Connection::write(TextMessage msg){
   boost::asio::post(*io_context_,
        [this, msg]()
        {
          bool writeInProgress = !pendingTextMessages_.empty();
          pendingTextMessages_.push_back(msg);
          if (!writeInProgress)
          {
            do_writeText();
          }
        });
}

void Connection::sendFile(std::string filePath){
	FileMessage fileMsg;
	if(fileMsg.loadFile(filePath))
		boost::asio::post(*io_context_,
			[this, fileMsg](){
			bool writeInProgress = !pendingFileMessages_.empty();
			pendingFileMessages_.push_back(fileMsg);
			if (!writeInProgress){
				do_writeFile();
			}
			});

}

void Connection::operator<<(std::string message){
	write(TextMessage(message));
}


void Connection::async_readMessage(){
	do_readHeader(*fileSocket_, fileMsg);
	do_readHeader(*textSocket_, textMsg);
	
}

void Connection::do_writeText(){
	TextMessage message = pendingTextMessages_.front();
	boost::asio::async_write(*textSocket_,
			boost::asio::buffer(message.getData(), message.getLength() + message.getHeaderSize()),
			[this](boost::system::error_code ec, std::size_t /*length*/){
			if (!ec){
				pendingTextMessages_.pop_front();
				if(!pendingTextMessages_.empty()){
					do_writeText();
				}
			}
			});
}

void Connection::writeFileCallback(const boost::system::error_code& ec, std::size_t bytesTransfered){
	FileMessage message = pendingFileMessages_.front();
/*	if(removeFlag){
		removeFlag = false;
		pendingFileMessages_.pop_front();
		bytesSent = 0;
		std::cout<<message.getFileName()<<" removed from the queue.\n";
		return;
	}*/
	if(pauseFlag){
		t->expires_after(boost::asio::chrono::seconds(1));
		t->async_wait([this](const boost::system::error_code& ec){writeFileCallback(ec, 0);});
		return;
	}
	if(ec){
		std::cout<<"Error occured while sending "<<message.getFileName()<<": "<<ec.message()<<std::endl;
		pendingFileMessages_.pop_front();
		do_writeFile();
	}
	bytesSent += bytesTransfered;
	auto messageSize = message.getLength() + message.getHeaderSize();
	if(bytesSent == messageSize ){
		std::cout<<message.getFileName()<<" sent."<<std::endl;
		pendingFileMessages_.pop_front();
		bytesSent = 0;
		if(pendingFileMessages_.empty())
			return;
		else
			do_writeFile();
	}
	fileSocket_->async_write_some(boost::asio::buffer(&buffer[bytesSent], messageSize - bytesSent),
			[this](const boost::system::error_code& e, std::size_t b){writeFileCallback(e, b);});
}

void Connection::do_writeFile(){
	removeFlag = false;
	pauseFlag = false;
	t = std::make_unique<boost::asio::steady_timer>(*io_context_, boost::asio::chrono::seconds(1));
	FileMessage message = pendingFileMessages_.front();
	buffer = message.getData();
	bytesSent = 0;
	fileSocket_->async_write_some(boost::asio::buffer(buffer, message.getLength() + message.getHeaderSize()),
			[this](const boost::system::error_code& e, std::size_t b){writeFileCallback(e, b);});
}

 void Connection::do_readHeader(ISocket& socket, Message& msg){
	boost::asio::async_read(socket,
        boost::asio::buffer(msg.getHeaderPtr(), msg.getHeaderSize()),
        [this, &msg, &socket](boost::system::error_code ec, std::size_t /*length*/){
          if (!ec){
		msg.setHeader();
		do_readBody(socket, msg);
          }
          else{
	  	std::cerr<<"Connection closed: "<<ec.message()<<"\n";
	//	this->close();
          }
        });
  }


void Connection::do_readBody(ISocket& socket, Message& msg){
	boost::asio::async_read(socket,
        boost::asio::buffer(msg.getBodyPtr(), msg.getLength()),
        [this, &msg, &socket](boost::system::error_code ec, std::size_t /*length*/){
          if (!ec){
		msg.show();
		do_readHeader(socket, msg);
          }
          else{
	  	std::cerr<<"Error while reading body: "<<ec.message()<<"\n";
	//	this->close();
          }
        });
}

void Connection::resumeFileTransfer(){
	pauseFlag = false;
}

void Connection::pauseFileTransfer(){
	pauseFlag = true;
}
