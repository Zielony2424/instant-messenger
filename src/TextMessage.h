#ifndef TEXTMESSAGE_H
#define TEXTMESSAGE_H
#include <string>
#include <iostream>
#include "Message.h"

class TextMessage : public Message{
	public:
	TextMessage(std::string body);
	TextMessage();
	void show() override;
};
#endif
