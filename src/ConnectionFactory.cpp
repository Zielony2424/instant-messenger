#include "ConnectionFactory.h"

ConnectionFactory::ConnectionFactory(boost::asio::io_context& io_context):IConnectionFactory(io_context){}

Connection ConnectionFactory::getConnection(std::string adress){
	tcp::socket textSocket(io_context_);
	tcp::socket fileSocket(io_context_);
	boost::asio::connect(textSocket, adressToIterator(adress,"1338"));	
	boost::asio::connect(fileSocket, adressToIterator(adress, "1339"));	
	std::cout<<"Established connection to "<<adress<<"\n";
	return Connection(&io_context_, std::move(socketToISocket(textSocket)), std::move(socketToISocket(fileSocket)));
}

