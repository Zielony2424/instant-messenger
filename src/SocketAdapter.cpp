#include "SocketAdapter.h"


SocketAdapter::SocketAdapter(tcp::socket socket):socket_(std::move(socket)){}
std::size_t SocketAdapter::send(const boost::asio::const_buffer& buffers){
	return socket_.send(buffers);
}
boost::asio::executor SocketAdapter::get_executor(){
	return socket_.get_executor();
}
void SocketAdapter::async_write_some(const boost::asio::const_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler) {
	socket_.async_write_some(buffers, handler);
}
void SocketAdapter::async_read_some(const boost::asio::mutable_buffer& buffers, std::function<void(const boost::system::error_code&, size_t)>&& handler) {
	socket_.async_read_some(buffers, handler);
}

bool SocketAdapter::is_open(){
	return socket_.is_open();
}

void SocketAdapter::close(){
	socket_.close();
}

